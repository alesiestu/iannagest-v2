<?php

namespace App\Controller;

use Google\Cloud\Firestore\FirestoreClient;

use Symfony\Component\Console\Command\Command as SymfonyCommand;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FrigoController extends AbstractController
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    Private $refUsers;
    public function __construct()
    {
        
   
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.__DIR__.'/food-89042-firebase-adminsdk-ll40h-851b677038.json');
        
     
    
        
        
    }

    /**
     * @Route("/frigo", name="frigo")
     */
    public function index()
    {
        $db = new FirestoreClient([
            'database' => '(default)',
        ]);
        $usersRef = $db->collection('temp');
        $snapshot = $usersRef->documents();
        
        return $this->render('frigo/index.html.twig', [
            'controller_name' => 'FrigoController',
            'data' => $snapshot
        ]);
    }

    
}
