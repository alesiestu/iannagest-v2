<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AppController extends Controller
{
    /**
     * @Route("/", name="app_default")
     */
    public function index()
    {
        //return $this->render('home/index.html.twig');
        return $this->redirectToRoute('admin_page');
    }

    


}
