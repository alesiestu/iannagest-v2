<?php

namespace App\Controller;

//use Kreait\Firebase;
//use Kreait\Firebase\ServiceAccount;
//use Morrislaptop\Firestore\Factory;

use Google\Cloud\Firestore\FirestoreClient;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ExitproductController extends AbstractController
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    Private $refUsers;
    public function __construct()
    {
        
   
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.__DIR__.'/food-89042-firebase-adminsdk-ll40h-851b677038.json');
       
       
    
        
        
    }
    /**
     * @Route("/exitproduct", name="exitproduct")
     */
    public function index()
    {
       
       
        $db = new FirestoreClient([
            'database' => '(default)',
        ]);
        $usersRef = $db->collection('uscita');
        $snapshot = $usersRef->documents();
       
       
       
       
        return $this->render('exitproduct/index.html.twig', [
            'controller_name' => 'ExitproductController',
            'data' => $snapshot
        ]);
    }
}
