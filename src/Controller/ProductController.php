<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Google\Cloud\Firestore\FirestoreClient;


class ProductController extends AbstractController
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    Private $refUsers;
    public function __construct()
    {
        
   
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.__DIR__.'/food-89042-firebase-adminsdk-ll40h-851b677038.json');
       
       
    
        
        
    }
    /**
     * @Route("/product", name="product")
     */
    public function index()
    {
        $db = new FirestoreClient([
            'database' => '(default)',
        ]);
        $usersRef = $db->collection('prodottifiniti');
        $snapshot = $usersRef->documents();
        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'data' => $snapshot
        ]);
    }

    /**
    * @Route("product/{id}/show-product", requirements={"id": "\d+"}, name="show-product")
    
    */
    public function show($id)
    {
           
        $int = (int)$id;
        $db = new FirestoreClient([
            'database' => '(default)',
        ]);
        
        $usersRef = $db->collection('lavorazione');
        
        $query = $usersRef->where('codicesessione', '=', $int);
        
        $snapshot = $query->documents();

      
        
        return $this->render('product/show.html.twig', [
             'data' => $snapshot
        ]);
    }

    
}
