<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Google\Cloud\Firestore\FirestoreClient;

class MateriaprimaController extends AbstractController
{


     /**
     * Create a new controller instance.
     *
     * @return void
     */
    Private $refUsers;
    public function __construct()
    {
        
   
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.__DIR__.'/food-89042-firebase-adminsdk-ll40h-851b677038.json');
       
       
    
        
        
    }
    
    /**
     * @Route("/materiaprima", name="materiaprima")
     */
    public function index()
    {
        $db = new FirestoreClient([
            'database' => '(default)',
        ]);
        $usersRef = $db->collection('ingresso');
        $snapshot = $usersRef->documents();
        return $this->render('materiaprima/index.html.twig', [
            'controller_name' => 'MateriaprimaController',
            'data' => $snapshot
        ]);
    }
}
