<?php

namespace App\Controller;



use Google\Cloud\Firestore\FirestoreClient;



use Symfony\Component\Console\Command\Command as SymfonyCommand;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


use Dompdf\Dompdf;
use Dompdf\Options;


class IngressoController extends AbstractController
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    Private $refUsers;
    public function __construct()
    {
        
   
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.__DIR__.'/food-89042-firebase-adminsdk-ll40h-851b677038.json');
 
        
    }
    



    /**
     * @Route("/ingresso", name="ingresso")
     */
    public function index()
    {
        
        $db = new FirestoreClient(
           // ['database' => '(default)',]
        );
        $usersRef = $db->collection('venditafinita');
        $snapshot = $usersRef->documents();
   
        return $this->render('ingresso/index.html.twig', [
            
            'data' => $snapshot
        ]);
    }

     /**
    * @Route("ingresso/{id}/mostra", requirements={"id": "\d+"}, )
    
    */
    public function show($id)
    {
           
        $int = (int)$id;
        $db = new FirestoreClient([
            'database' => '(default)',
        ]);
        
        $usersRef = $db->collection('uscita');
        
        $query = $usersRef->where('codicesessionevendita', '=', $int);
        
        $snapshot1 = $query->documents();



      
        
        return $this->render('ingresso/show.html.twig', [
             'datauscita' => $snapshot1
        ]);
    }


    /**
    * @Route("{id}/dettagli", requirements={"id": "\d+"}, )
    
    */
    public function show2($id)
    {
           
        $int = (int)$id;
        $db = new FirestoreClient([
            'database' => '(default)',
        ]);
        
        $usersRef = $db->collection('lavorazione');
        
        $query = $usersRef->where('codicesessione', '=', $int);
        
        $snapshot1 = $query->documents();

        
       
      
        
        return $this->render('ingresso/dettagli.html.twig', [
             'datauscita' => $snapshot1
        ]);
    }

    /**
    * @Route("/{id}/{id2}/{nome}")
    
    */
    public function riepilogo(Request $request, $id, $id2,$nome)
    {
       // $int = (int)$id;
       // $two = (int)$id2;

      //  $int = $request->query->get('id');
       // $two = $request->query->get('id2');
        
       // dump([$id,$id2]);
        $int = (int)$id;
        $two = (int)$id2;

       
        $db2 = new FirestoreClient([
            'database' => '(default)',
        ]);

        //query uscita da id
        $uscitadati = $db2->collection('venditafinita');
        $queryuscita = $uscitadati->where('codicevendita', '=', $int);
        $snapshotuscita = $queryuscita->documents();


        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->setIsRemoteEnabled(true);

        
        $dompdf = new Dompdf($pdfOptions);
        
        
     



        


        
        
       
       
       
       
        $db = new FirestoreClient([
            'database' => '(default)',
        ]);

       
        
       
       
       
       
        $usersRef = $db->collection('lavorazione');
        
       
        $query = $usersRef->where('codicesessione', '=', $two);
        
       
        $snapshot1 = $query->documents();



        $html = $this->renderView('pdf/mypdf.html.twig', [
            'title' => "Welcome to our PDF Test",
            'clientiuscita'=>$snapshotuscita, 
            'datiprodotti' => $snapshot1,
            'nome'=>$nome,
        ]);

        $dompdf->loadHtml($html);
        
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => true
        ]);



      
        
        return $this->render('ingresso/riepilogo.html.twig', [
            'clientiuscita'=>$snapshotuscita, 
            'datiprodotti' => $snapshot1,
            'nome'=>$nome,
           
        ]);
    }

    


    

    

}
