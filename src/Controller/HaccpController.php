<?php

namespace App\Controller;

use Google\Cloud\Firestore\FirestoreClient;

use Symfony\Component\Console\Command\Command as SymfonyCommand;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HaccpController extends AbstractController
{
    
    
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    Private $refUsers;
    public function __construct()
    {
        
   
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.__DIR__.'/food-89042-firebase-adminsdk-ll40h-851b677038.json');
        
     
    
        
        
    }
    
    
    
    
    
    /**
     * @Route("/haccp", name="haccp")
     */
    public function index()
    {
        
        $db = new FirestoreClient([
            'database' => '(default)',
        ]);
        $usersRef = $db->collection('controllogiornalierogenerale');
        $snapshot = $usersRef->documents();
        
        
        
        return $this->render('haccp/index.html.twig', [
            'data' => $snapshot
        ]);
    }


    /**
    * @Route("haccp/{id}/mostra", requirements={"id": "\d+"}, )
    
    */
    public function show($id)
    {
           
        $int = (int)$id;
        $db = new FirestoreClient([
            'database' => '(default)',
        ]);
        
        $usersRef = $db->collection('controllogiornaliero');
        
        $query = $usersRef->where('codicesessione', '=', $int);
        
        $snapshot1 = $query->documents();

      
        
        return $this->render('haccp/show.html.twig', [
             'datauscita' => $snapshot1
        ]);
    }
}
